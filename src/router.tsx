import { lazy } from "react";
import { BrowserRouter, Routes, Route, HashRouter } from "react-router-dom";
import { RootTemplate } from "./components/root-template";

const AlphabetPage = lazy(() => import('./features/alphabet').then((module) => ({ default: module.AlphabetPage })));
const MainPage = lazy(() => import('./features/main').then((module) => ({ default: module.MainPage })));
const DonatePage = lazy(() => import('./features/donate').then((module) => ({ default: module.DonatePage })));

export const Router = () => {
    return (
        <HashRouter>
            <RootTemplate>
                <Routes>
                    <Route
                        path="/alipbi"
                        element={<AlphabetPage />}
                    />
                    <Route
                        path="/donate"
                        element={<DonatePage />}
                    />
                    <Route
                        path="*"
                        element={<MainPage />}
                    />
                </Routes>
            </RootTemplate>
        </HashRouter>
    );
}