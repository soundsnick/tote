import './assets/index.css';
import 'react-toastify/dist/ReactToastify.css';
import 'bootstrap-4-grid';

import { connectLogger, createCtx } from '@reatom/framework';
import { reatomContext } from '@reatom/npm-react';
import React from 'react';
import ReactDOM from 'react-dom/client';

import App from './App';

const store = createCtx();
connectLogger(store);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <reatomContext.Provider value={store}>
      <App />
    </reatomContext.Provider>
  </React.StrictMode>
);
