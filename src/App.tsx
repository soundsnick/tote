import './assets/index.css';
import { Router } from './router';
import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <>
      <Router />
      <ToastContainer
        position='bottom-center'
      />
    </>
  );
}

export default App;
