import styled from "@emotion/styled";

export const Container = styled.div`
    max-width: 900px;
    margin: auto;
    position: relative;
    padding: 0 15px;
`;