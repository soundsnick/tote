import { lazy } from "react";

const Card = lazy(() => import("antd/es/card"));

export const Footer = ({ ...rest }) => {
    return (
        <Card {...rest}>
            <p className="text-center">Хабарласу: <a href="https://t.me/yarnduffold">Telegram</a></p>
        </Card>
    );
}