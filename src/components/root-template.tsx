import styled from "@emotion/styled";
import { FC, HTMLAttributes, lazy, Suspense, useMemo } from "react";
import { Link, useLocation } from "react-router-dom";
const Dropdown = lazy(() => import("antd/es/dropdown"));
const Layout = lazy(() => import("antd/es/layout"));
const LayoutContent = lazy(() => import("antd/es/layout/index").then((module) => ({ default: module.default.Content })));
const Space = lazy(() => import("antd/es/space"));
const Spin = lazy(() => import("antd/es/spin"));
const MenuOutlined = lazy(() => import("@ant-design/icons/MenuOutlined"));

import { Container } from "./container";
import { Footer } from "./footer";

const items = [
    {
        label: <Link to="/">Кириллица ⟷ Төте жазу</Link>,
        key: '/',
    },
    {
        label: <Link to="/alipbi">Әліпби</Link>,
        key: '/alipbi',
    },
    // {
    //     label: <Link to="/donate">Донат</Link>,
    //     key: '/donate',
    // },
];

const ContainerStyled = styled(Container)`
    display: flex;
    align-items: center;
    margin: 20px auto;
    @media only screen and (max-width: 900px) {
        flex-direction: column-reverse;
        .menu-wrapper {
            margin-top: 20px;
        }
        .subtitle {
            display: none;
        }
    }
`;

export const RootTemplate: FC<HTMLAttributes<HTMLDivElement>> = ({ children }) => {
    const location = useLocation();
    
    const title = useMemo(() => items.find((n) => n?.key === location.pathname)?.label, [items, location]);

    return (
        <Layout style={{ height: '100vh', overflow: 'auto', padding: '50px 0', display: 'flex', flexDirection: 'column' }}>
            <div>
                <ContainerStyled>
                    <div className="menu-wrapper">
                        <Dropdown menu={{ items, selectable: true, selectedKeys: [location.pathname] }} trigger={['click']}>
                            <a onClick={(e) => e.preventDefault()}>
                                <Space>
                                    <MenuOutlined />
                                    Мәзір
                                </Space>
                            </a>
                        </Dropdown>
                    </div>
                    <div className="col text-center">
                        <h1 className="mb-0">ءتوتە جازۋ (Төте жазу)</h1>
                        <h4 className="mt-3 subtitle" style={{ fontWeight: 500 }}>{title}</h4>
                    </div>
                </ContainerStyled>
            </div>
            <LayoutContent style={{ flex: 1 }}>
                <Container className="pb-5">
                    <Suspense fallback={
                        <div className="text-center py-5">
                            <Spin />
                        </div>
                    }>
                        {children}
                    </Suspense>
                    <Footer className='mt-2' />
                </Container>
            </LayoutContent>
        </Layout>
    );
}