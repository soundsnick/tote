import { lazy } from "react";
const Card = lazy(() => import("antd/es/card"));
const Tooltip = lazy(() => import("antd/es/tooltip"));

import { cyrToToteBase } from "../../core/mapping";
import { GridStyled } from "./styled";

export const Component = () => {
    return (
        <>
            <Card>
                <p>А.Байтұрсынұлы қоғам, ғылым және мәдениеттің өркендеуі жазу арқылы іске асатынын жақсы түсінді. Ғалым сауатсыздықты жою үшін әріп, оқулық мәселесін кезек күттірмейтін мәселе екенін біліп, 1910 жылдан бастап, араб жазуының қазақ тілі үшін қолайлы емес жақтарын түзеп, оны тілдің дыбыстық ерекшеліктеріне сай етіп, сингармониялық ұлттық әліпби түзеді. Бұған дейін, Х-ғасырдан бері парсы-араб жазуын өзгеріссіз қолданып келгенбіз. Сөйтіп, А.Байтұрсынұлы полиграфиялық жағынан қолайлы-қолайсыз жерлерін, оқыту процесіндегі тиімді-тиімсіз жақтарын таразылай отырып, қазақ тілінің төл дыбыстарына ғана тән 28 әріптен тұратын әліпби құрастырды.</p>
                <p style={{ fontWeight: 500 }}>Нақтысында, әліпби 24 таңбадан және дәйекшеден тұрады. Егер, "ء" дәйекше сөз алдына қойылса, сол сөздегі барлық дауысты дыбыстар жіңішке деп түсінілген. Мысалы: تۇردى - тұрды, ءتۇردى - түрді.</p>
                <p>Ақпарат дереккөзі: <a href="https://massaget.kz/blogs/23884/">https://massaget.kz/blogs/23884/</a></p>
            </Card>
            <Card className="mt-2">
                <GridStyled>
                    {Object.entries(cyrToToteBase).map(([cyrillic, arabic]) => (
                        <Tooltip key={`${cyrillic}-${arabic}`} title={arabic.name}>
                            <Card className={'text-center'}>
                                <h2>{arabic.tote}</h2>
                                <p>{cyrillic}</p>
                            </Card>
                        </Tooltip>
                    ))}
                </GridStyled>
            </Card>
        </>
    );
}