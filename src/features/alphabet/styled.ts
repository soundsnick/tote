import styled from "@emotion/styled";

export const GridStyled = styled.div`
    display: grid;
    grid-template-columns: repeat(5, auto);
    gap: 5px;
    @media only screen and (max-width: 900px) {
        grid-template-columns: repeat(3, auto);
    }
`;