import { lazy } from "react";
import { useAction, useAtom } from "@reatom/npm-react";
const Button = lazy(() => import("antd/es/button"));
const Card = lazy(() => import("antd/es/card"));
const TextArea = lazy(() => import("antd/es/input/TextArea"));
const CloseCircleOutlined = lazy(() => import("@ant-design/icons/CloseCircleOutlined"));
const CopyOutlined = lazy(() => import("@ant-design/icons/CopyOutlined"));
import CopyToClipboard from "react-copy-to-clipboard";
import { notifications } from "../../core/notifications";

import { cyrillicInputAtom, cyrillicToToteAtom, onCyrillicInputChange, onReset, onToteInputChange, toteInputAtom, toteToCyrillicAtom } from "./state";

export const Component = () => {
    const [cyrillicValue] = useAtom(cyrillicInputAtom);
    const [toteValue] = useAtom(toteInputAtom);
    const setCyrillic = useAction(onCyrillicInputChange);
    const setTote = useAction(onToteInputChange);
    const reset = useAction(onReset);

    const [toteResult] = useAtom(cyrillicToToteAtom);
    const [cyrillicResult] = useAtom(toteToCyrillicAtom);

    return (
        <>
            <Card>
                <div className="position-relative">
                    <TextArea style={{ minHeight: 200 }} className="mt-3" placeholder="Кириллица" value={toteValue?.length > 0 ? cyrillicResult : cyrillicValue} onChange={setCyrillic} />
                    <CopyToClipboard text={toteValue?.length > 0 ? cyrillicResult : cyrillicValue} onCopy={notifications.copied}>
                        <Button style={{ position: 'absolute', bottom: 5, left: 5 }}>
                            <CopyOutlined />
                        </Button>
                    </CopyToClipboard>
                </div>
                
                <div className="position-relative">
                    <TextArea style={{ minHeight: 200 }} dir="rtl" className="mt-3" placeholder="ٴتوتە" value={cyrillicValue?.length > 0 ? toteResult : toteValue} onChange={setTote} />
                    <CopyToClipboard text={cyrillicValue?.length > 0 ? toteResult : toteValue} onCopy={notifications.copied}>
                        <Button style={{ position: 'absolute', bottom: 5, right: 5 }}>
                            <CopyOutlined />
                        </Button>
                    </CopyToClipboard>
                </div>
                <div className="mt-2 text-center">
                    <Button danger type="primary" onClick={reset}>
                        <CloseCircleOutlined />
                        Өшіру
                    </Button>
                </div>
            </Card>
        </>
    );
}