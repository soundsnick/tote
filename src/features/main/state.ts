import { action, atom } from "@reatom/framework";
import { withReset } from '@reatom/lens';
import { cyrToTote, cyrToToteLight, lighter } from "../../core/mapping";

export const cyrillicInputAtom = atom('').pipe(withReset());
export const toteInputAtom = atom('').pipe(withReset());

const getKeyByValue = (object: Record<string, string>, value: string) => {
    return Object.keys(object).find(key => object[key] === value);
}

export const cyrillicToToteAtom = atom((ctx) => {
    const words = ctx.spy(cyrillicInputAtom)?.split(' ');
    if (words?.length) {
        return words.reduce((accWords, wordRaw) => {
            const word = wordRaw.toLowerCase();
            const val = word.split('');
            return [...accWords, val.reduce((acc, n) => {
                if (n in cyrToToteLight) {
                    return {
                        isLight: true,
                        result: [acc.isLight ? undefined : lighter, acc.result, cyrToToteLight[n as keyof typeof cyrToToteLight]].join('')
                    };
                } else if (cyrToTote[n as keyof typeof cyrToTote]) {
                    return {
                        ...acc,
                        result: `${acc.result}${cyrToTote[n as keyof typeof cyrToTote]}`
                    }
                } else {
                    return {
                        ...acc,
                        result: `${acc.result}${n}`
                    }
                }
            }, { result: '', isLight: false }).result];
        }, [] as string[]).join(' ');
    } 
    return '';
    
}).pipe(withReset());

export const toteToCyrillicAtom = atom((ctx) => {
    const words = ctx.spy(toteInputAtom)?.split(' ');
    if (words?.length) {
        return words.reduce((accWords, wordRaw) => {
            const word = wordRaw.toLowerCase();
            const val = word.split('');
            return [...accWords, val.reduce((acc, n) => {
                if (n === lighter) {
                    return { ...acc, isLight: true };
                } else {
                    
                    if (Object.values(cyrToToteLight).includes(n) && acc.isLight) {
                        return {
                            ...acc,
                            result: [acc.result, getKeyByValue(cyrToToteLight, n)].join('')
                        };
                    } else if (getKeyByValue(cyrToTote, n)) {
                        return {
                            ...acc,
                            result: `${acc.result}${getKeyByValue(cyrToTote, n)}`
                        }
                    } else {
                        return {
                            ...acc,
                            result: `${acc.result}${n}`
                        }
                    }
                }
            }, { result: '', isLight: false }).result];
        }, [] as string[]).join(' ');
    } 
    return '';  
}).pipe(withReset());

export const onCyrillicInputChange = action((ctx, event) => {
    cyrillicInputAtom(ctx, event.currentTarget.value);
    toteInputAtom.reset(ctx);
});

export const onToteInputChange = action((ctx, event) => {
    toteInputAtom(ctx, event.currentTarget.value);
    cyrillicInputAtom.reset(ctx);
});

export const onReset = action((ctx) => {
    toteInputAtom.reset(ctx);
    cyrillicInputAtom.reset(ctx);
});