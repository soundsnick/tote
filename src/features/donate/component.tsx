import { lazy } from "react";

import { toast } from 'react-toastify';
import CopyToClipboard from "react-copy-to-clipboard";
import { notifications } from "../../core/notifications";
const Button = lazy(() => import("antd/es/button"));
const Card = lazy(() => import("antd/es/card"));
const CopyOutlined = lazy(() => import("@ant-design/icons/CopyOutlined"));

export const Component = () => {
  

    return (
        <>
            <Card>
                <h1>Рахмет айту!</h1>
                <pre className="p-0 m-0" style={{ fontFamily: 'inherit', whiteSpace: 'pre-wrap' }}>
                    {`Ресурс толықтай тегін. 
Жоба тек энтузиазммен жасалып жатқанынан, бүкіл шығыны өзімде (шығыны айтарлықтай көп те емес).
                    
Сонда да қолдау білдіргіңіз келсе, мұның екі жолы бар:
    1. Ой. Кеңес. Жылы лебіз. Осының бірі сізде бар болса - менің телеграм парақшама жазсаңыз болады. 
    2. Қаржылай рахмет. Шай мен майға 😄
`}
                </pre>
                <div className="p-0 mt-5 text-left">
                    <b>Kaspi:</b> 4400 4301 2234 4168 
                    <i className="d-inline-block ml-1" style={{ color: 'grey' }}>(Ерназар К.)</i>
                    <CopyToClipboard text={'4400430122344168'} onCopy={notifications.copied}>
                        <Button className="ml-2">
                            <CopyOutlined />
                        </Button>
                    </CopyToClipboard>
                    <br />
                    <b>Halyk:</b> 4405 6397 4389 4238
                    <CopyToClipboard text={'4405639743894238'} onCopy={notifications.copied}>
                        <Button className="ml-2">
                            <CopyOutlined />
                        </Button>
                    </CopyToClipboard>
                </div>
                <p className="mt-5" style={{ borderLeft: `4px solid grey`, paddingLeft: 15 }}>Хабарламасына “Төтеден” деп түртіп қойсаңыз 🙏</p>
            </Card>
        </>
    );
}