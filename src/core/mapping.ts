export const cyrToToteBase = {
    'а': { tote: 'ا', name: 'әліп'},
    'б': { tote: 'ب', name: 'ба'},
    'в': { tote: 'ﯚ', name: 'уау құсбелгі'},
    'г': { tote: 'گ', name: 'гәф'},
    'ғ': { tote: 'ع', name: 'айн'},
    'д': { tote: 'د', name: 'дәл'},
    'ж': { tote: 'ج', name: 'жійм'},
    'з': { tote: 'ز', name: 'зайн'},
    'и': { tote: 'ي', name: 'я екі ноқат'},
    'к': { tote: 'ك', name: 'кәф'},
    'қ': { tote: 'ق', name: 'қаф'},
    'л': { tote: 'ل', name: 'ләм'},
    'м': { tote: 'م', name: 'мійм'},
    'н': { tote: 'ن', name: 'нұн'},
    'ң': { tote: 'ڭ', name: 'кәф үш ноқат'},
    'о': { tote: 'و', name: 'уау'},
    'п': { tote: 'پ', name: 'па'},
    'р': { tote: 'ر', name: 'ре'},
    'с': { tote: 'س', name: 'сыйн'},
    'т': { tote: 'ت', name: 'та'},
    'у': { tote: 'ۋ', name: 'уау үш ноқат'},
    'ұ': { tote: 'ۇ', name: 'уау дамма'},
    'ф': { tote: 'ف', name: 'фа'},
    'х': { tote: 'ح', name: 'ха'},
    'һ': { tote: 'ھ', name: 'һә екі көз'},
    'ч': { tote: 'چ', name: 'ха үш ноқат'},
    'ш': { tote: 'ش', name: 'шыйн (сыйн үш ноқат)'},
    'ы': { tote: 'ى', name: 'сыйн'},
    'е': { tote: 'ە', name: 'һә'},
};

export const cyrToTote = {
    ...Object.entries(cyrToToteBase).reduce((acc, [name, value]) => {
        return {
            ...acc,
            [name]: value.tote
        }
    }, {} as Record<keyof typeof cyrToToteBase, string>),
    'й': 'ي',
    'э': 'ە',
    'ю': 'يۋ',
    'я': 'يا',
    'ц': 'تس',
    'щ': 'ش',
}

export const cyrToToteLight = {
    'ә': cyrToTote['а'],
    'ө': cyrToTote['о'],
    'ү': cyrToTote['ұ'],
    'і': cyrToTote['ы'],
};

export const lighter = 'ء';